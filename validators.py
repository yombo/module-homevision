"""
Validate Homevision input types
"""
from yombo.lib.inputtypes.integer import Integer


class Homevision_io_port(Integer):
    MIN = 0
    MAX = 16
    CONVERT = True

    def validate(self, value, **kwargs):
        return super(Homevision_io_port, self).validate(value, **kwargs)
