Summary
=======

This module extends the capabilities of the [Yombo Gateway](https://yombo.net/)
by adding support for Homevision and Homevision Pro devices.

The goal is support all to functions: X10, relays, input and outputs, 1-wire temperature
sensors, and more.

Additional details about this module at: https://yombo.net/Modules/homevision

Learn about [Yombo Gateway](https://yombo.net/) or
[Get started today](https://yombo.net/Docs/Gateway/Quick_start)

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download and install this module automatically.

Requirements
============

To be used with the Yombo Gateway. If X-10 control is desired, install the
X10 API module.

License
=======

The [Yombo](https://yombo.net/) team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See LICENSE file for full details.
