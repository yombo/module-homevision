"""
Provide Homevision support.

The goal is support all to function, macros, I/O interfaces, 1-wire, etc.

This module can interface with X10 devices if the X10Api module is installed.

For details: https://yombo.net/Modules/homevision

License
=======

Feel free to use or copy under the MIT license.

The Yombo team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.


.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: 2012-2017 Yombo
:license: YRPL 1.6
"""

# Import python libraries
from collections import deque
from curses.ascii import iscntrl, unctrl, ascii, ascii as get_ascii
from string import ascii_uppercase
from serial import SerialException

# Import twisted libraries
from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.serialport import SerialPort
from twisted.internet.task import LoopingCall

from yombo.core.log import get_logger
from yombo.core.module import YomboModule
from yombo.utils import is_yes_no, percentage
from yombo.utils.maxdict import MaxDict
from yombo.ext.expiringdict import ExpiringDict

logger = get_logger("modules.homevision")
    
class Homevision(YomboModule):
    """
    A homevision or homevision-pro device is required.  This
    module expects to access the device using serial port
    or a usb<->serial converter.  The buildint homevision-pro
    usb connection qualifies for this.
    """
    @inlineCallbacks
    def _init_(self, **kwargs):
        """
        Gets the module setup for use. Defines basic variables.
        """
        self.received_commands = MaxDict(200) # stores incoming commands for status update when done.
        self.x10cmds = {} # command instances received from x10api
        self.x10stack = {} # used to track house/unit codes before a command.
        self.lastHouseCode = None  # Track last x10 house codes received

        self.monitored_input_ports = ExpiringDict(max_len=100, max_age_seconds=120)
        self.monitored_output_ports = ExpiringDict(max_len=100, max_age_seconds=120)

        self.HVSerial = None #the serial connection
        self.HVProtocol = None #for sending commands to HV

        self.startable = False # track when load has completed...
        self.x10support = False # Disable x10 by default, will enable if x10api found
        self._hvStatus = yield self._SQLDict.get(self, '_hvStatus')
#        self._hvStatus = {}

        self.X10Commands = {
          'ON'             : {'cmd': '01', 'type' :"X"}, #force on
          'OFF'            : {'cmd': '03', 'type' :"X"}, #force off
          'DIM'            : {'cmd': '06', 'type' :"X"}, #force dim
          'BRIGHTEN'       : {'cmd': '0C', 'type' :"X"}, #force brighten
          'SET_LEVEL'      : {'cmd': '11', 'type' :"P"}, #set to level ##
          'SEND_ADDRESS'   : {'cmd': '15', 'type' :"X"}, #only send house/unit
          'SET_PRESET_DIM1': {'cmd': '1D', 'type' :"P"}, #only send house/unit

          'HOUSE_ON'         : {'cmd': '1602', 'type' :"P"},
          'HOUSE_OFF'        : {'cmd': '160A', 'type' :"P"},
          'HOUSE_BRIGHTEN'   : {'cmd': '1606', 'type' :"P"},
          'HOUSE_DIM'        : {'cmd': '160E', 'type' :"P"},
#          'HOUSE_LIGHTS_ON'  : {'cmd': '1604', 'type' :"P"},  #doesn't seem to work?
#          'HOUSE_LIGHTS_OFF' : {'cmd': '1600', 'type' :"P"},  #doesn't seem to work?
          'EXTENDED_CODE'    : {'cmd': '3C', 'type' :"g"},
          'STATUS_REQUEST' : {'cmd': '1609', 'type' :"P"},
          'STATUS_ON'      : {'cmd': '1607', 'type' :"P"},
          'STATUS_OFF'     : {'cmd': '1601', 'type' :"P"},
          'HAIL_REQUEST'   : {'cmd': '160D', 'type' :"P"},
          'HAIL_ACK'       : {'cmd': '1605', 'type' :"P"},

          'READ_CURRENT_STATE': {'cmd': 'iE', 'type' :"X"}, #HV stored value
          'MICRO_DIM'         : {'cmd': '23', 'type' :"X"}, #only send house/unit
          'MICRO_BRIGHTEN'    : {'cmd': '22', 'type' :"X"} } #only send house/unit

        self.x10HouseCodes = {
          'A' : '0',
          'B' : '1',
          'C' : '2',
          'D' : '3',
          'E' : '4',
          'F' : '5',
          'G' : '6',
          'H' : '7',
          'I' : '8',
          'J' : '9',
          'K' : 'A',
          'L' : 'B',
          'M' : 'C',
          'N' : 'D',
          'O' : 'E',
          'P' : 'F',
          }

        self.x10RevHouseCodes = {
          '0' : 'A',
          '1' : 'B',
          '2' : 'C',
          '3' : 'D',
          '4' : 'E',
          '5' : 'F',
          '6' : 'G',
          '7' : 'H',
          '8' : 'I',
          '9' : 'J',
          'A' : 'K',
          'B' : 'L',
          'C' : 'M',
          'D' : 'N',
          'E' : 'O',
          'F' : 'P',
          }
        
        self.HVCommands = {
          'X10_ON'             : {'cmd': '01', 'type' :"X", 'result':"28"}, #force on
          'X10_OFF'            : {'cmd': '03', 'type' :"X", 'result':"28"}, #force off
          'X10_DIM'            : {'cmd': '06', 'type' :"X", 'result':"28"}, #force dim
          'X10_BRIGHTEN'       : {'cmd': '0C', 'type' :"X", 'result':"28"}, #force brighten
          'X10_SET_LEVEL'      : {'cmd': '11', 'type' :"P", 'result':"20"}, #set to level ##
          'X10_SEND_ADDRESS'   : {'cmd': '15', 'type' :"X", 'result':"28"}, #only send house/unit
          'X10_SET_PRESET_DIM1': {'cmd': '1D', 'type' :"P", 'result':"20"}, #only send house/unit

          'X10_HOUSE_ON'         : {'cmd': '1602', 'type' :"P", 'result':"20"},
          'X10_HOUSE_OFF'        : {'cmd': '160A', 'type' :"P", 'result':"20"},
          'X10_HOUSE_BRIGHTEN'   : {'cmd': '1606', 'type' :"P", 'result':"20"},
          'X10_HOUSE_DIM'        : {'cmd': '160E', 'type' :"P", 'result':"20"},
          'X10_HOUSE_LIGHTS_ON'  : {'cmd': '1604', 'type' :"P", 'result':"20"},
          'X10_HOUSE_LIGHTS_OFF' : {'cmd': '1600', 'type' :"P", 'result':"20"},
          'X10_EXTENDED_CODE'    : {'cmd': '3C', 'type' :"g", 'result':"37"},
          'X10_STATUS_REQUEST' : {'cmd': '1609', 'type' :"P", 'result':"20"},
          'X10_STATUS_ON'      : {'cmd': '1607', 'type' :"P", 'result':"20"},
          'X10_STATUS_OFF'     : {'cmd': '1601', 'type' :"P", 'result':"20"},
          'X10_HAIL_REQUEST'   : {'cmd': '160D', 'type' :"P", 'result':"20"},
          'X10_HAIL_ACK'       : {'cmd': '1605', 'type' :"P", 'result':"20"},

          'X10_READ_CURRENT_STATE': {'cmd': '1E', 'type' :"X", 'result':"28"}, #HV stored value
          'X10_MICRO_DIM'         : {'cmd': '23', 'type' :"X", 'result':"28"}, #only send house/unit
          'X10_MICRO_BRIGHTEN'    : {'cmd': '22', 'type' :"X", 'result':"28"}, #only send house/unit

          'INPUTPORT_DISABLE'   : {'cmd': '03', 'type' :":", 'result':"0A"},
          'INPUTPORT_ENABLE'    : {'cmd': '04', 'type' :":", 'result':"0A"},
          'INPUTPORT_READSTATE' : {'cmd': '05', 'type' :":", 'result':"0A"},
          'INPUTPORT_ENABLELOWTRIGGER'   : {'cmd': '07', 'type' :":", 'result':"0A"},
          'INPUTPORT_DISABLELOWTRIGGER' : {'cmd': '08', 'type' :":", 'result':"0A"},
          'INPUTPORT_ENABLEHIGHTRIGGER'   : {'cmd': '09', 'type' :":", 'result':"0A"},
          'INPUTPORT_DISABLEHIGHTRIGGER' : {'cmd': '0A', 'type' :":", 'result':"0A"},

          'OUTPUTPORT_SETLOW'    : {'cmd': '00', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_SETHIGH'   : {'cmd': '01', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_TOGGLE'    : {'cmd': '02', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_DIABLE'    : {'cmd': '03', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_ENABLE'    : {'cmd': '04', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_READSTATE' : {'cmd': '05', 'type' :"3", 'result':"03"},
          'OUTPUTPORT_PULSEHIGH' : {'cmd': '07', 'type' :"Z", 'result':"03"},
          'OUTPUTPORT_PULSELOW'  : {'cmd': '08', 'type' :"Z", 'result':"03"},

          'MACRO_RUN'     : {'cmd': '00', 'type' :"O", 'result':""},
          'MACRO_ENABLE'  : {'cmd': '01', 'type' :"O", 'result':""},
          'MACRO_DISABLE' : {'cmd': '02', 'type' :"O", 'result':""},
          } #more commands to come...

        self._connectionAttempts = 0  # number of attempts made to connect to serial (usb) port.
        self._connectionInterval = 4  # how many seconds between retries.
        self._connected = False  # true if HV is responsive
        self._hasStarted = False # true if self.start has been called

        self.HVDevices = {'inputport':{}, # map of HV device to Yombo Devices.
                          'outputport':{},
                          'x10':{},
                         }

        if "outputport" not in self._hvStatus:
            self._hvStatus['outputport'] = {0:{'state' : 0, 'enabled' :0},
                                          1:{'state' : 0, 'enabled' :0},
                                          2:{'state' : 0, 'enabled' :0},
                                          3:{'state' : 0, 'enabled' :0},
                                          4:{'state' : 0, 'enabled' :0},
                                          5:{'state' : 0, 'enabled' :0},
                                          6:{'state' : 0, 'enabled' :0},
                                          7:{'state' : 0, 'enabled' :0},
                                         }
        if "inputport" not in self._hvStatus:
            self._hvStatus['inputport'] =  {0:{'state' : 0, 'enabled' :0},
                                          1:{'state' : 0, 'enabled' :0},
                                          2:{'state' : 0, 'enabled' :0},
                                          3:{'state' : 0, 'enabled' :0},
                                          4:{'state' : 0, 'enabled' :0},
                                          5:{'state' : 0, 'enabled' :0},
                                          6:{'state' : 0, 'enabled' :0},
                                          7:{'state' : 0, 'enabled' :0},
                                         }

        # print "homevision device types: %s" % self._ModuleDeviceTypes()
        # print "homevision devices: %s" % self._ModuleDevices()

    def _load_(self, **kwargs):
        """
        Checks to see if the X10API module is installed. If not, disables
        X10 support through the Homevision board. Inits the connection
        to Homevision.
        """
        self._reload_(type='all')
        self._startConnection()

    def _startConnection(self):
        """
        Make the actual connection to Homevision device..
        """
        hv_serial_port = self._ModuleVariables['port']['values'][0]
        # print "homevision: self._ModuleVariables: %s" % variables
        try:
            self.HVSerial = SerialPort(HomevisionProtocol(self), hv_serial_port, reactor, baudrate='19200')

            self._connected = True
        except SerialException as error:
            self._connectionAttempts += 1
            if self._connectionAttempts % 20 == 1:
                logger.warn("Unable to connect to Homevision serial port '{port}'. Will continue trying. Attempt: {attempts}, Reason: {error}",
                            attempts=self._connectionAttempts,
                            error=error,
                            port=hv_serial_port)
            reactor.callLater(self._connectionInterval, self._startConnection)
#        print "homevision attempting connection to serial, done: %s" % self._connected

    def _reload_(self, **kwargs):
        try:
            self.X10APIModule = self._Modules['X10API']
            self.x10support = True
#            logger.warn("aaaa3: {mdt}", mdt=self.X10APIModule._DeviceTypes)
            self.HVDevices['x10'] = self.X10APIModule.x10_devices
        except:
            # no X10API module!
            logger.warn("Homevision - Disable suppot for X10 - X10API module not found!")
            self.x10support = False

    def _start_(self, **kwargs):
        """
        Get the status of the HV board. Maybe something changed?
        """
        self._hasStarted = True

    # def _device_platforms_(self, **kwargs):
    #     """
    #     Collect any special device platform classes.
    #
    #     :param kwargs:
    #     :return:
    #     """
    #     return [
    #         ['yombo.modules.homevision.classes', 'hv_output_port'],
    #     ]
    def _input_type_validators_(self, **kwargs):
        """
        Called to collect any input type validators.

        :param kwargs:
        :return:
        """
        return [
            ['yombo.modules.homevision.validators', 'Homevision_io_port'],
        ]

    def homevision_x10api_interfaces(self, **kwargs):
        """
        This is a hook implemented by the x10 api module. This simply tells the x10 API module that we can support
        x10 device interactions.
        
        :param kwargs: 
        :return: 
        """
        try:
            priority = self._ModuleVariables['port']['values'][0]
        except:
            priority = 0

        logger.debug("Registering Homevision with X10 API is priority 0 for X10 commands.")
        return {'priority': priority, 'callback': self.x10api_send_command}

    def homevision_voicecmds_load(self, **kwargs):
        """
        hook_voicecmds_load called by the voicecommands library to get a list of voice commands to add for processing.

        :param kwargs:
        :return:
        """
        return [
            {'voice_cmd': "homevision [reset]", 'order' : 'nounverb'}
        ]

    def x10api_send_command(self, x10cmd):
        """
        Called by the X10API module to send commands. This is defined by the X10API module on requirements.
        """
        logger.debug("Homevision received X10 command: {command}", command=x10cmd)
        if self.x10support is not True:
            return x10cmd.failed("Homevision cannot support x10 commands directly. Must be sent through x10api module.")

        request_id = x10cmd.request_id

        self.x10cmds[request_id] = x10cmd
        self.received_commands[request_id] = {
            'callback': self.x10api_command_done,
            'device': x10cmd.deviceobj,
            'command': x10cmd.cmdobj,
            'request_id': request_id
        }

        house = x10cmd.x10_house['value']
        number = int(x10cmd.x10_number['value'] - 1)
        command = x10cmd.cmdobj
        cmd = "X10_%s" % command.cmd
        cmd = cmd.upper()

        house = self._houseToHex(house)
        number = "%x" % number

        hvcmd = "%s%s%s%s" % (self.HVCommands[cmd]['type'], house, number, self.HVCommands[cmd]['cmd'])
        logger.debug("HV Sendx10cmd to _send: {hvcommand}", hvcommand=hvcmd)
        self.HVSerialSend(hvcmd, request_id=x10cmd.request_id)

    def x10api_command_done(self, request_id):
        """
        Called by homevision when done.
        """
        if request_id in self.x10cmds:
            self.x10cmds[request_id].done()
            del self.x10cmds[request_id]

    def device_command_done(self, request_id):
        """
        Done processing command received by _device_command_.  Lets send a status update.
        
        :param request_id:
        :return:
        """
        data = self.received_commands[request_id]
        device = data['device']
        command = data['command']

        device_type = self._DeviceTypes[device.device_type_id]

        cmd = command.machine_label
        if device_type.machine_label == 'x10_lamp': # basic lamp
            if cmd == 'on':
                machine_status = 1
                human_status = 'On'
            elif cmd == 'off':
                machine_status = 0
                human_status = 'Off'
        elif device_type.machine_label == 'x10_lamp': # basic lamp
            logger.debug("in x10 lamp")
            if cmd == 'on':
                machine_status = 1
                human_status = '100%'
            elif cmd == 'off':
                machine_status = 0
                human_status = 'Off'
            elif cmd == 'dim':
                if type(device.status[0]['status']) is int:
                    machine_status = percentage(device.status_history[0]['machine_status'] - 12 / 100)
                    human_status = str(int(machine_status*100)) + '%'
                else:
                    machine_status = 0.88
                    human_status = '88%'
            elif cmd == 'bright':
                if type(device.status[0]['status']) is int:
                    machine_status = percentage(device.status_history[0]['machine_status'] + 12 / 100)
                else:
                    machine_status = 100
                    human_status = '100%'

            if type(machine_status) is int:
                if machine_status > 1:
                    machine_status = 1
                    human_status = '100%'
                elif machine_status < 0:
                    machine_status = 0
                    human_status = 'Off'
            else:
                machine_status = 0
                human_status = 'Off'
        elif device_type.machine_label == 'hv_output_port':  # basic lamp
            if cmd == 'open':
                machine_status = 0
                human_status = 'Open'
            elif cmd == 'close':
                machine_status = 1
                human_status = 'Closed'
        else:
            print(device_type.machine_label)
            machine_status = None
            human_status = None


        human_message = "%s is now %s. " % (device.area_label, human_status.lower())

        device.device_command_done(request_id, message=human_message)
        device.set_status(human_status=human_status,
                          human_message=human_message,
                          machine_status=machine_status,
                          command=command,
                          request_id=request_id,
                          reported_by=self._FullName)

    def _device_command_(self, **kwargs):
        """
        Implements the system hook to process commands.

        This will only process commands for the homevision system. X10 commands and 1-wire must come from an API
        based module.
        """
        device = kwargs['device']
        request_id = kwargs['request_id']

        if self._is_my_device(device) is False:
            logger.debug("Homevision module cannot handle device_type_id: {device_type_id}", device_type_id=device.device_type_id)
            return False

        if self.HVProtocol.homevision_ok == False:
            device.device_command_failed(
                request_id,
                message="Problem with Homevision connection, requested command could not be completed"
            )
            return False


        command = kwargs['command']
        self.received_commands[request_id] = {
            'request_id': request_id,
            'callback': self.device_command_done,
            'device': device,
            'command': command,
        }

        device_variables = device.device_variables
        device_type_id = device.device_type_id
        module_device_types = self._ModuleDeviceTypes()

        device_type = module_device_types[device_type_id]

        if device_type.machine_label == 'hv_input_port':
            hvdevice = device_variables['port']['values'][0]
            if command.machine_label == 'enable':
                item = 'INPUTPORT_DISABLE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'disable':
                item = 'INPUTPORT_ENABLE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'status':
                item = 'INPUTPORT_READSTATE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'enablelowtrigger':
                item = 'INPUTPORT_ENABLELOWTRIGGER'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'disablelowtrigger':
                item = 'INPUTPORT_DISABLELOWTRIGGER'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'enablehightrigger':
                item = 'INPUTPORT_ENABLEHIGHTRIGGER'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'disablehightrigger':
                item = 'INPUTPORT_DISABLEHIGHTRIGGER'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])

        elif device_type.machine_label == 'hv_output_port':
            hvdevice = device_variables['port']['values'][0]
            if command.machine_label == 'open': # SETLOW (open)
                if self._hvStatus['outputport'][int(hvdevice)]['enabled'] == 1:
                    item = 'OUTPUTPORT_SETLOW'
                    hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
                else:
                    device.device_command_failed(request_id, message="Port disabled, enable before use.")
                    return False
            elif command.machine_label == 'close': # SETHIGH (close)
                if self._hvStatus['outputport'][int(hvdevice)]['enabled'] == 1:
                    item = 'OUTPUTPORT_SETHIGH'
                    hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
                else:
                    device.device_command_failed(request_id, message="Port disabled, enable before use.")
                    return False
            elif command.machine_label == 'toggle':
                if self._hvStatus['outputport'][int(hvdevice)]['enabled'] == 1:
                    item = 'OUTPUTPORT_TOGGLE'
                    hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
                else:
                    device.device_command_failed(request_id, message="Port disabled, enable before use.")
                    return False
            elif command.machine_label == 'disable':
                item = 'OUTPUTPORT_DIABLE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'enable':
                item = 'OUTPUTPORT_ENABLE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'status':
                item = 'OUTPUTPORT_READSTATE'
                hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
            elif command.machine_label == 'pulse_high': # pulsehigh
                if self._hvStatus['outputport'][int(hvdevice)]['enabled'] == 1:
                    item = 'OUTPUTPORT_PULSEHIGH'
                    hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
                else:
                    device.device_command_failed(request_id, message="Port disabled, enable before use.")
                    return False
            elif command.machine_label == 'pulse_low': # pulse low
                if self._hvStatus['outputport'][int(hvdevice)]['enabled'] == 1:
                    item = 'OUTPUTPORT_PULSELOW'
                    hvcmd = "%s%02d%s" % (self.HVCommands[item]['type'], int(hvdevice), self.HVCommands[item]['cmd'])
                else:
                    device.device_command_failed(request_id, message="Port disabled, enable before use.")
                    return False

        elif device_type.machine_label == 'hv_macro': # Homevision Macros # todo: doesn't work yet.
            hvmacro = device_variables['macro_name']['values'][0]
            if command.machine_label == 'run': # run
                item = 'MACRO_RUN'
                hvcmd = "%s%s" % (self.HVCommands[item]['type'], hvmacro, self.HVCommands[item]['cmd'])
            elif command.machine_label == 'enable': # enable
                item = 'MACRO_ENABLE'
                hvcmd = "%s%s" % (self.HVCommands[item]['type'], hvmacro, self.HVCommands[item]['cmd'])
            elif command.machine_label == 'disble': # disable
                item = 'MACRO_DISABLE'
                hvcmd = "%s%s" % (self.HVCommands[item]['type'], hvmacro, self.HVCommands[item]['cmd'])

        else: # no valid command!
            device.device_command_failed(request_id, message="Homevision doesn't have support for: %s" % device_type.machine_label)
            return False

        #        logger.debug("HV Sending command: %s", hvcmd)
        self.HVSerialSend(hvcmd, request_id)
        return True

    def _device_status_(self, **kwargs):
        input_device = kwargs['device']
        status_type = None

    def HVSerialSend(self, cmd, request_id=None):
        """
        Send a command to homevision. When it's done, the callback will be called with the request_id.

        :param cmd: The string command to send to homevision.
        :type cmd: string
        :param request_id: The request_id for tracking.
        :type request_id: string
        """
        self.HVProtocol.sendQueueAdd(cmd, request_id)

    def check_io_port_has_device(self, port_type, port):
        """
        Checks if a homevision input port has a device. Returns device_id (due to caching).
        
        :param port: 
        :return: 
        """
        if port in self.monitored_input_ports:
            try:
                return self._Devices[self.monitored_input_ports[port]]
            except:
                del self.monitored_input_ports[port]
                pass
        devices = self._DeviceTypes.devices_by_device_type(port_type)
        for device_id, device in devices.items():
            if int(device.device_variables['port']['values'][0]) == int(port):
                self.monitored_input_ports[port] = device_id
                return device
        return None

    def HVSerialOutput(self, request_id, result, lastCommand):
        """
        Called when we receive output from homevision serial.
        """
        # logger.debug("HVSerialOutput: {request_id}, {result}, {lastCommand}", request_id=request_id, result=result, lastCommand=lastCommand)
        if request_id in self.received_commands:
            logger.debug("request_id in self.sendQueueMessages = {msg}", msg=self.received_commands[request_id])
        msgStatus = ''
        msgStatusExtra = ''
        textStatus = ''
        lastCommand = str(lastCommand)
        status_updated = False
    
        statusExtra = None
        textStatus = None

        a = result.split(" ")

#03 Cmd: 01 Done
#98 Input Port Changed: #00 High

#if it's a response to a command, process response
#else, process a status update, no command
        if lastCommand != '':  # Process command from incomming message!
          if a[0] == "03": #output port
            port = int(lastCommand[1:3])   #30701
            cmd = lastCommand[3:5]   #30701
            stateBefore = -2
            enabledBefore = -2
            if cmd == '00': #set low (open)
                stateBefore = self._hvStatus['outputport'][port]['state']
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['state'] = 0
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['state'] = -1
            elif cmd == '01': #set high (closed)
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['state'] = 1
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['state'] = -1
            elif cmd == '02': #toggle
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    if self._hvStatus['outputport'][port]['state'] == 1:
                        self._hvStatus['outputport'][port]['state'] = 0
                    else:
                        self._hvStatus['outputport'][port]['state'] = 1
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['state'] = -1
            elif cmd == '03': #disable
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['enabled'] = 0
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['enabled'] = -1
            elif cmd == '04': #enable
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['enabled'] = 1
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['enabled'] = -1
            elif cmd == '05': # status
                if a[3] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    if a[2] == '00':  #low & disabled
                        self._hvStatus['outputport'][port]['state'] = 0
                        self._hvStatus['outputport'][port]['enabled'] = 0
                    if a[2] == '01':  #low & enabled
                        self._hvStatus['outputport'][port]['state'] = 0
                        self._hvStatus['outputport'][port]['enabled'] = 1
                    if a[2] == '02':  #high & disabled
                        self._hvStatus['outputport'][port]['state'] = 1
                        self._hvStatus['outputport'][port]['enabled'] = 0
                    if a[2] == '03':  #high & enabled
                        self._hvStatus['outputport'][port]['state'] = 1
                        self._hvStatus['outputport'][port]['enabled'] = 1
                    # logger.debug("set output status: {port}", port=self._hvStatus['outputport'][port])
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."

            if self._hvStatus['outputport'][port]['state'] == 0:  #low
               recordStatus = "open"
            else:
               recordStatus = "close"
            if port in self.HVDevices['outputport']: # only do status if we have port assigned
                status, statusExtra = self._getPortStatus(self._hvStatus['outputport'][port])
                # logger.debug("self.HVDevices['outputport'][port].status[0] ({status}) != status ({otherStatus}) ...  statusExtra ({statusExtra})", status=self.HVDevices['outputport'][port].status[0].status, otherStatus=status, statusExtra=statusExtra)
                if self.HVDevices['outputport'][port].status[0].status != status or \
                  self.HVDevices['outputport'][port].status[0].statusextra != statusExtra:
                    self.HVDevices['outputport'][port].set_status(status = status,
                                                                  statusExtra = statusExtra,
                                                                  reported_by=self._FullName)

          elif a[0] == "0A": #input port
            stateBefore = -2
            enabledBefore = -2

            port = int(lastCommand[1:3])   #:0705
            cmd = lastCommand[3:5]  
            if cmd == '03': #disable
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['enabled'] = 0
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['enabled'] = -1
            elif cmd == '04': #enable
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    self._hvStatus['outputport'][port]['enabled'] = 1
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
                    self._hvStatus['outputport'][port]['enabled'] = -1
            elif cmd == '05': # status
                if a[3] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    if a[2] == '00':  #low(open) & disabled
                        self._hvStatus['inputport'][port]['state'] = 0
                        self._hvStatus['inputport'][port]['enabled'] = 0
                    if a[2] == '01':  #low & enabled
                        self._hvStatus['inputport'][port]['state'] = 0
                        self._hvStatus['inputport'][port]['enabled'] = 1
                    if a[2] == '02':  #high(closed) & disabled
                        self._hvStatus['inputport'][port]['state'] = 1
                        self._hvStatus['inputport'][port]['enabled'] = 0
                    if a[2] == '03':  #high & enabled
                        self._hvStatus['inputport'][port]['state'] = 1
                        self._hvStatus['inputport'][port]['enabled'] = 1
#                    logger.debug("set input status: %s", self._hvStatus['inputport'][port])
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."

            if self._hvStatus['inputport'][port]['state'] == 0:  #low
               recordStatus = "open"
            else:
               recordStatus = "close"

            if port in self.HVDevices['inputport']: # only do status if we have port assigned
                status, statusExtra = self._getPortStatus(self._hvStatus['inputport'][port])
                logger.debug("self.HVDevices['inputport'][port].status[0] ({Status}) != status ({statusOther})...statusExtra {statusExtra}", status=self.HVDevices['inputport'][port].status[0].status, statusOther=status, statusExtra=statusExtra)
                if self.HVDevices['inputport'][port].status[0].status != status or \
                  self.HVDevices['inputport'][port].status[0].statusextra != statusExtra:
                    self.HVDevices['inputport'][port].set_status(status = status,
                                                                 statusExtra = statusExtra,
                                                                 reported_by=self._FullName)

          elif a[0] == "28": #x10 device, response to type "X" (x10) commands.
            logger.debug("lastcommand: {lastCommand}", lastCommand=lastCommand)
            cmd = lastCommand[3:5]   # X0103 (A2 - Force off)
            house = self.x10RevHouseCodes[lastCommand[1:2].upper()]
            unit = int(lastCommand[2:3], 16)
            unit += 1
            address = "%s%d" % (house, unit)
            if cmd == '01': # Force On
                if a[2] == "Done":
                    textStatus = "Command completed successfully."
                    msgStatus = "done"
                    logger.debug("set {address} to on", address=address)
                    self.X10APIModule.status_update(house=house, unit=unit, command='on', deviceObj=self.received_commands[request_id]['device'])
#        if request_id != "INTERNALMESSAGECONTROL":
#            logger.debug("HVSerialOutput2 = {msg}", msg=self.sendQueueMessages[request_id].payload)

                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."
            elif cmd == '03': # Force Off
                if a[2] == "Done":
                    msgStatus = "done"
                    logger.debug("set {address} to off", address=address)
                    self.X10APIModule.status_update(house=house, unit=unit, command='off', deviceObj=self.received_commands[request_id]['device'])
                    self.received_commands[request_id]['callback'](request_id)
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."

          elif a[0] == "17": # Controller commands
            cmd = lastCommand[1:3]
            if cmd == '00': # Check communications
                if a[2] == "Done":
                    msgStatus = "done"
                    textStatus = "Command completed successfully."
                    recordStatus = 'HV Ok'
                else:
                    msgStatus = "failed"
                    textStatus = "Homevision reported and error processing request."

          #now lets tell the sender we are done!
          if request_id != "INTERNALMESSAGECONTROL" and request_id != '':
              if request_id in self.received_commands:
                self.received_commands[request_id]['callback'](request_id)
                del self.received_commands[request_id]

        else: # response not from a command, it's a status from HV unsoliciated.
          if a[0] == '98':  ## Import port
                            # 0    1    2        3   4    5
                            #98 Input Port Changed: #00 High
            portNum =  int(a[4][-2:])
            if a[5].lower() == "high":
                self._hvStatus['inputport'][portNum]['state'] = 1
                machine_status = 1
                human_status = 'High'
            else:
                self._hvStatus['inputport'][portNum]['state'] = 0
                machine_status = 0
                human_status = 'Low'

            device = self.check_io_port_has_device('hv_input_port', portNum)
            if device is not None:
                device.set_status(human_status=human_status,
                                  human_message='Input port is %s' % human_status,
                                  machine_status=machine_status,
                                  reported_by=self._FullName)

          elif a[0] == '90':  ## X10 house/unit received (no command)
                              # 0        2        3 4 5
                              #90 X-10 House/Unit : A 1
            house = a[4]
            if house != self.lastHouseCode:
                self.x10stack.clear()
            self.lastHouseCode = house
            unit = a[5]
            address = "%s%s" % (house, unit)
            if address in self.HVDevices['x10']:
              if house in self.x10stack:
                self.x10stack[house].append(unit)
              else:
                self.x10stack[house] = [unit, ]

          elif a[0] == '91':  ## X10 house/function received (no unit, applies to all units in stack
                              # 0    1     2      3 4  5
                              #91 X-10 House/Func : A On
            house = a[4]
            if house != self.lastHouseCode:
                self.x10stack.clear()
            self.lastHouseCode = house
            command = ' '.join(a[5:])
            if house in self.x10stack:
              for unit in self.x10stack[house]:
                logger.warn("set {house}{unit} to: {command}", house=house, unit=unit, command=command)
                self.X10APIModule.status_update(house=house, unit=unit, command=command)

    def _getPortStatus(self, arecord):
        status = None
        extra = {}
        if arecord['state'] == 0:  #low
           status = "open"
        else:
           status = "close"

        if arecord['enabled'] == 0: # disabled
           extra = {'enabled':"0"}
        else:
           extra = {'enabled':"1"}
        return status, extra

    def _houseToHex(self, house):
        return self.x10HouseCodes[house]

    def _hexToHouse(self, house):
        ascii_uppercase[int(house, 16)]


class HomevisionProtocol(Protocol):
    def __init__(self, hvfactory):
        self.name = "Homevision.protocol"
        self.description = "Responsible for communication with homevision over USB/Serial."
        self.author = "Mitch Schwenk @ Yombo"
        self.url = "https://yombo.net"

        self.hvfactory = hvfactory  #homevision module from above
        self.__buffer = ''
        self.line_mode = 1
        self.delimiter = '^M^J^A'
        self.MAX_LENGTH = 16384
        self.paused = False
        self.sendQueue = deque()
        self.sendWaiting = False
        self.statusCheckTimer = LoopingCall(self.sendStatusCheck)
        self.checkQueueTimer = LoopingCall(self.sendQueueCheck)
        self.hvfactory.HVProtocol = self
        self._lastCommand = '' # last command sent to HV
        self.homevision_ok = False
        self.toSendResults = deque()
        self.connected = False

    def connectionFailed(self):
        logger.warn("Homevision connection failed!!")
        self.connected = False

    def connectionMade(self):
        logger.debug("Homevision connection complete. Getting status.")
        self.connected = True
        self.statusCheckTimer.start(120, False)
        self.checkQueueTimer.start(15)

        _statusCommands = [
        'G00', # status of the HV unit
        30005, 30105, 30205, 30305, 30405, 30505, 30605, 30705, #output ports
        ':0005', ':0105', ':0205', ':0305', ':0405', ':0505', ':0605', ':0705', #output ports
        ]
        
        for cmd in _statusCommands:
            self.sendQueueAdd(cmd, "INTERNALMESSAGECONTROL")
        
    def sendStatusCheck(self):
        self.sendLine('INTERNALMESSAGECONTROL', "G00")  #say hello to homevision

    def dataReceived(self, newdata):
        """
        Received raw data from serial port. Calls self.lineReceived when actual data is received.
        :param newdata: A new bit of data.
        :return:
        """
        # print "dataReceived: %s" % newdata   # bad idea to print this!!!
        data = ''
        for c in newdata:
            if iscntrl(c):
                data = "%s%s" % (data, unctrl(c))
            else:
                data = "%s%s" % (data, chr(c))
        self.__buffer = "%s%s" % (self.__buffer, data)
        while self.line_mode and not self.paused:
            try:
                line, self.__buffer = self.__buffer.split(self.delimiter, 1)
            except ValueError:
                if len(self.__buffer) > self.MAX_LENGTH:
                    line, self.__buffer = self.__buffer, ''
                    return self.lineLengthExceeded(line.decode())
                break
            else:
                linelength = len(line)
                if linelength > self.MAX_LENGTH:
                    exceeded = line + self.__buffer
                    self.__buffer = ''
                    return self.lineLengthExceeded(exceeded)
                why = self.lineReceived(line)
                if why or self.transport and self.transport.disconnecting:
                    return why
        else:
            if not self.paused:
                data=self.__buffer
                self.__buffer=''
                if data:
                    return self.rawDataReceived(data)

    def lineLengthExceeded(self, line):
        """
        Called when the maximum line length has been reached.
        Override if it needs to be dealt with in some special way.
        """
        return line.ConnectionLost('Line length exceeded')

    def lineReceived(self, line):
        """
        Data from self.dataReceived(). This is called for each line of data received from Homevision.
        :param line: A line of text.
        :return:
        """
        line = line.strip()
        a = line.split(" ")
        logger.debug("Line received: {line}({line_type}) ({last_request_id}) - {lastCommand}", line=line, line_type=type(line), last_request_id=self.last_request_id, lastCommand=self._lastCommand)
        appendIt = False

        if a[0] == "17": # HV status check result "G"
            if a[2] == "Done" and self._lastCommand == "G00":
                self.homevision_ok = True
            else:
                self.homevision_ok = False
            if self.last_request_id != "INTERNALMESSAGECONTROL":
                appendIt = True
        else:
            appendIt = True

        self.sendWaiting = False

        # self.sendQueueCheck(False)
        if appendIt is True: # don't actuall send results. Just add to the queue for processing by sendQueueCheck()
            self.toSendResults.append({'request_id':self.last_request_id, 'line':line, 'command':self._lastCommand})

        self.last_request_id = ''
        self._lastCommand = ''
        self.sendQueueCheck()

    def sendQueueAdd(self, cmd, request_id):
        # logger.debug("HV sendqueueadd({cmd})", cmd=cmd)
        self.sendQueue.append({'request_id':request_id, 'cmd': cmd})
        self.sendQueueCheck()

    def sendQueueCheck(self, allowResults=True):
        """
        First checks to see if there is a send queue to send to homevision. If there isn't, then send any pending results
        back to to the Homevision factory.

        Todo: This can probably be optimized better.
        :param allowResults: If True, then allow results from previous received lines from homevision to be sent to factory.
        :return: None
        """
        # logger.info("sendQueueCheck was just called. len(self.sendqueue:{lensend}), self.sendWaiting:{sendwaiting}",
        #            lensend=len(self.sendQueue), sendwaiting=self.sendWaiting)
        logger.debug("sendQueueCheck was just called. self.sendqueue:{sendQueue}", sendQueue=self.sendQueue)
        if self.connected is False:
            return
        if allowResults is True and len(self.toSendResults) > 0:
            item = self.toSendResults.popleft()
#            logger.warn("???????????????????????????????? {item}", item=item)
            self.hvfactory.HVSerialOutput(item['request_id'], item['line'], item['command'])
            self.sendQueueCheck()
        elif len(self.sendQueue) > 0 and self.sendWaiting is False:
            self.sendWaiting = True
            qitem = self.sendQueue.popleft()
            self.sendLine(qitem['request_id'], qitem['cmd'])
            self.sendQueueCheck()

    def sendLine(self, request_id, line):
        """
        Send a line out to homevision.
        :param request_id: A reference to request_id. Can be used to later to match a response from HV to a request.
        :param line: The line of data to send to HV.
        :return: None
        """
        self.sendWaiting = True
        self.last_request_id = request_id
        self._lastCommand = line
        if self.statusCheckTimer.running:
            self.statusCheckTimer.reset()
        logger.debug("Sendline to homevision: request_id: {last_request_id}   command: {lastCommand}",
                     lastCommand=self._lastCommand, last_request_id=self.last_request_id)
        outline = ",%s\r\n" % line
        self.transport.write(outline.encode())


